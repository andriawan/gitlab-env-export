#!/bin/bash

content='#!/bin/bash\n'

for single_env in `env`; do 
    # echo "echo export $fn >> env.sh" >> ./env.sh
    content+="export $single_env\n"
done

echo "$content" > env.dev.sh

cat env.dev.sh
